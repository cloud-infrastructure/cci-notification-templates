CCI Notification Templates
==========================

Variables that can be used:

$reference       # Short message to be kept as a reference on the disabled compute node
$vmsprojects     # List of affected VMs
$date            # Date of the scheduled intervention
$hour            # Hour of the scheduled intervention
$new_date        # when rescheduling an intervention, the new date
$new_hour        # when rescheduling an intervention, the new hour
$vmcount         # Number of vms affected
$otg             # Reference to a SNOW ticket. INC, RQF or OTG
